package org.bedu.shopbedu.interfaces

import org.bedu.shopbedu.MyProductItem
import retrofit2.Call
import retrofit2.http.GET


interface ApiInterface {
    @GET("products/")
    fun getProducts():Call <MutableList<MyProductItem>>
}