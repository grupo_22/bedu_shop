package org.bedu.shopbedu

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration

class MyApp: Application()  {

    override fun onCreate() {
        super.onCreate()

        Realm.init(this)
        val configuration = RealmConfiguration.Builder()
            .name("Carrito.db")
            .deleteRealmIfMigrationNeeded()
            .build()

        Realm.setDefaultConfiguration(configuration)
    }
}