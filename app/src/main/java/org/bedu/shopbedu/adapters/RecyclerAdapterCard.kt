package org.bedu.shopbedu.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import org.bedu.shopbedu.R
import org.bedu.shopbedu.models.Card



class RecyclerAdapterCard(
    private val context: Context,
    private val cards: List<Card>,
    private val itemClickListener: OnCardClickListener,

    ) : RecyclerView.Adapter<RecyclerAdapterCard.ViewHolder>(){

    interface OnCardClickListener{
        fun onItemClick(cards: Card)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_card, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cards.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.cardView.animation = AnimationUtils.loadAnimation(holder.cardView.context,
            R.anim.slide_product
        )
        val cards = cards[position]
        holder.bind(cards,context)
        holder.view.setOnClickListener{ itemClickListener.onItemClick(cards) }
    }

    class ViewHolder(val view : View) : RecyclerView.ViewHolder(view) {
        val cardView = view.findViewById(R.id.card) as CardView
        private val backGraund = view.findViewById(R.id.cardBackgraund) as ConstraintLayout
        private val textColorCard = view.findViewById(R.id.textCard) as TextView
        private val textbanco = view.findViewById(R.id.banco) as TextView
        private val textvence = view.findViewById(R.id.vencimiento) as TextView
        private val textfecha = view.findViewById(R.id.fecha) as TextView

        fun bind(cards :Card, context: Context) {

            backGraund.setBackgroundColor(cards.backGraund)
            textColorCard.setTextColor(cards.textColorCard)
            textbanco.setTextColor(cards.textColorCard)
            textvence.setTextColor(cards.textColorCard)
            textfecha.setTextColor(cards.textColorCard)

        }
    }
}