package org.bedu.shopbedu.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import org.bedu.shopbedu.R
import org.bedu.shopbedu.models.Carrito
import java.math.RoundingMode
import java.text.DecimalFormat

class RecyclerAdapterCarrito(
    private val context: Context,
    private val carrito: MutableList<Carrito>,
    private val itemClickListener : OnCarritoClickListener,

    ): RecyclerView.Adapter<RecyclerAdapterCarrito.ViewHolder>() {


    interface OnCarritoClickListener{
        fun onNextClick(carrito: Carrito)
        fun onBeforeClick(carrito: Carrito)
        fun onDeleteClick(carrito: Carrito)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_carrito, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return carrito.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val carrito = carrito[position]
        holder.bind(carrito, context)
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        private val image = view.findViewById(R.id.imgProduct_carrito) as ImageView
        private val text = view.findViewById(R.id.product_name_carrito) as TextView
        private val price = view.findViewById(R.id.product_price_carrito) as TextView
        private val stock = view.findViewById(R.id.stock_carrito) as TextView
        private val next = view.findViewById(R.id.textnext) as ImageView
        private val before = view.findViewById(R.id.textbefore) as ImageView
        private val delete = view.findViewById(R.id. delete) as ImageView



        fun bind(carrito: Carrito, context: Context) {
            val df = DecimalFormat("####.##")
            df.roundingMode = RoundingMode.CEILING
            price.text = "$ " + (df.format(carrito.price?.times(carrito.stock!!))).toString()
            next.setOnClickListener { itemClickListener.onNextClick(carrito) }
            before.setOnClickListener { itemClickListener.onBeforeClick(carrito) }
            delete.setOnClickListener {  itemClickListener.onDeleteClick(carrito) }
            Picasso.get().load(carrito.image).into(image)
            text.text = carrito.title
            stock.text = carrito.stock.toString()
        }
    }
}
