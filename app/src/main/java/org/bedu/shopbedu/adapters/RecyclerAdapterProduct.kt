package org.bedu.shopbedu.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import org.bedu.shopbedu.R
import org.bedu.shopbedu.MyProductItem

class RecyclerAdapterProduct(
    var product: MutableList<MyProductItem>,
): RecyclerView.Adapter<RecyclerAdapterProduct.ViewHolder>() {

     lateinit var  itemClickListener : OnProductClickListener

    fun setValue(product: MutableList<MyProductItem>){
        this.product = product
        notifyDataSetChanged()
    }



    interface OnProductClickListener{
        fun onProductClick(product: MyProductItem, imageView: ImageView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_product, parent, false))
    }

    override fun getItemCount(): Int {
        return product.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.cardView.animation = AnimationUtils.loadAnimation(holder.cardView.context,
            R.anim.slide_product
        )

        val product = product[position]
        holder.bind(product)
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val productName = view.findViewById(R.id.product_name) as TextView
        val price = view.findViewById(R.id.product_price) as TextView
        val image = view.findViewById(R.id.imgProduct) as ImageView
        val starRating = view.findViewById(R.id.ratingBar_detalle) as RatingBar
        val ratingVal = view.findViewById(R.id.rtVal) as TextView
        val cardView = view.findViewById(R.id.card) as CardView

        fun bind(product: MyProductItem) {
            image.transitionName = product.id.toString()
            view.setOnClickListener {
                itemClickListener.onProductClick(product,image)
            }
            productName.text = product.title
            price.text = "$ " + product.price.toString()
            ratingVal.text = product.rating!!.count.toString()
            starRating.rating =  product.rating.rate.toFloat()
            Picasso.get().load(product.image).into(image)
        }
    }
}