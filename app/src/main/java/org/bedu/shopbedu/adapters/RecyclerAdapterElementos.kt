package org.bedu.shopbedu.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.bedu.shopbedu.R
import org.bedu.shopbedu.models.Elemento

class RecyclerAdapterElementos(
    private val context:Context,
    private val elementos: List<Elemento>,
    private val itemClickListener: OnElementosClickListener,

    ) : RecyclerView.Adapter<RecyclerAdapterElementos.ViewHolder>(){

    interface OnElementosClickListener{
        fun onItemClick(elementos: Elemento)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_elemento, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return elementos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val elementos = elementos[position]
        holder.bind(elementos,context)
        holder.view.setOnClickListener{ itemClickListener.onItemClick(elementos) }
    }

    class ViewHolder(val view : View) : RecyclerView.ViewHolder(view) {
        private val icon = view.findViewById(R.id.icon_elemento) as ImageView
        private val text = view.findViewById(R.id.text_elemento) as TextView

        fun bind(elementos: Elemento, context: Context) {
            icon.setImageResource(elementos.icon)
            text.text = elementos.text
        }
    }
}

