package org.bedu.shopbedu.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import org.bedu.shopbedu.R
import org.bedu.shopbedu.models.Card
import org.bedu.shopbedu.models.Metod


class RecyclerAdapterMetod(
    private val context: Context,
    private val metodos: List<Metod>,
    private val itemClickListener: OnMetodClickListener,
    ) : RecyclerView.Adapter<RecyclerAdapterMetod.ViewHolder>(){

    interface OnMetodClickListener{
        fun onItemClick(metodos: Metod)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_metodo, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return metodos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.cardView.animation = AnimationUtils.loadAnimation(holder.cardView.context,
            R.anim.slide_product
        )
        val metodos = metodos[position]
        holder.bind(metodos,context)
        holder.view.setOnClickListener{ itemClickListener.onItemClick(metodos) }
    }

    class ViewHolder(val view : View) : RecyclerView.ViewHolder(view) {
        val cardView = view.findViewById(R.id.card) as CardView
        private val textColorCard = view.findViewById(R.id.imageMetod) as ImageView

        fun bind(metodo : Metod, context: Context) {
            textColorCard.setImageResource(metodo.logoImage)
        }
    }
}