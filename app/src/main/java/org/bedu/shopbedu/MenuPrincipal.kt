package org.bedu.shopbedu

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.Toast
import androidx.core.view.forEach
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.navOptions

import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.menu_principal_activity.*
import org.bedu.shopbedu.fragments.LoginFragment


class MenuPrincipal : AppCompatActivity() {

    lateinit var preferences: SharedPreferences

    val options3 = navOptions {
        anim {
            enter = R.anim.slide_in_bot
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu_principal_activity)
        // Instanciamos nuestro NavHostFragment y nuestro navController que nos permitirá realizar la navigation entre fragments.
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.navigation_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        val bottomMenu = findViewById<BottomNavigationView>(R.id.bottomMenu)




        // Navegamos entre nuestros fragments según el itemId seleccionado estableciendo las animaciones para la transición previamente definidas (options, options2, options3)
        bottomMenu.setOnItemSelectedListener() { item ->
            when (item.itemId) {
                R.id.home_dest -> {
                    navController.navigate(R.id.listFragment, null)
                    true
                }
                R.id.perfil_dest -> {
                    navController.navigate(R.id.perfilFragment,null, options3)
                    true
                }
                R.id.carrito_dest -> {
                    navController.navigate(R.id.carritoFragment,null, options3)
                    true
                }
                else -> {
                    true
                }
            }
        }
        lifecycleScope.launchWhenResumed {
            navController.addOnDestinationChangedListener { _, destination, _ ->
                when (destination.id) {
                    R.id.listFragment,R.id.perfilFragment,R.id.carritoFragment -> {
                        bottomMenu.animate().apply {
                            translationY(0f)
                            duration = 700
                            interpolator = AccelerateDecelerateInterpolator()
                            start()
                        }
                        bottomMenu.menu.forEach { it.isEnabled = true }
                    }
                    else -> {
                       bottomMenu.menu.forEach { it.isEnabled = false }
                        bottomMenu.animate().apply {
                            translationY(170f)
                            duration = 700
                            interpolator = AccelerateDecelerateInterpolator()
                            start()
                        }

                    }
                }
            }
        }

    }


    // Permite realizar diferentes acciones según que item del Menu sea elegido.
    // Uri Documentation → https://developer.android.com/reference/android/net/Uri
    override fun onOptionsItemSelected(item: MenuItem): Boolean{
        return when (item.itemId) {
            R.id.ayuda -> {
                val url = "https://bedu.org";
                startActivity(Intent(Intent.ACTION_VIEW).apply {
                    data = Uri.parse(url)
                })
                true
            }
            R.id.lupa -> {
                Toast.makeText(applicationContext, R.string.search_not_available, Toast.LENGTH_SHORT).show()
                true
            }
            R.id.loginOff ->{
                setValues()
                goToLogged()
                true
            }
            else -> {
                true
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    fun setValues(){
        preferences =  getSharedPreferences(LoginFragment.PREFS_NAME, Context.MODE_PRIVATE)
        preferences.edit()
            .putBoolean(LoginFragment.IS_LOGGED, false)
            .apply()
    }

    fun goToLogged(){
        //cambiar de actividad sin poder regresar a esta con back button
        val i = Intent(this, MainActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
    }
}