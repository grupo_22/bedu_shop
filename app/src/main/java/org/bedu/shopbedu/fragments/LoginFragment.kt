package org.bedu.shopbedu.fragments


import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import org.bedu.shopbedu.R
import org.json.JSONObject


class LoginFragment : Fragment() {

    //Esta url es la que nos da bedu
    private val Url = "https://reqres.in/api/login/"
    //Pero usamos esta porque trae todos los usuarios disponibles y no tenemos que ir entre paginas
    private  val Url2 = "https://reqres.in/api/users?per_page=20"

    //guardamos en cache
    companion object {
        val JSON = "JSON"
        val ID = "ID"
        val PREFS_NAME = "org.bedu.login"
        val IS_LOGGED = "is_logged"
    }
     lateinit var preferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //variables tratar de cambiar declaracion!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        val linkRegistro = view.findViewById(R.id.linkRegister) as TextView
        val btnLogin = view.findViewById(R.id.btnLogin) as Button
        val inputEmail = view.findViewById(R.id.inputEmail) as TextInputLayout
        val inputPassword = view.findViewById(R.id.inputPassword) as TextInputLayout


        //Instanciamos nuestra variable en la cual obtenemos nuestros datos guardados en cache
        preferences =  requireActivity().getSharedPreferences( PREFS_NAME, Context.MODE_PRIVATE) // lo google y salio con requireActivity() porque? no lo sabremos nunca

        //boton para volver a login
        linkRegistro.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_register)
        }

        btnLogin.setOnClickListener {
            //if (inputEmail.editText?.text.toString().isNotEmpty() && inputPassword.editText?.text.toString().isNotEmpty()) {
            when(LoginChecker.validateLoginInput(inputEmail.editText?.text.toString(),inputPassword.editText?.text.toString())){
                "EMPTY_FIELDS" -> Snackbar.make(view.findViewById(R.id.loginLayout), R.string.missingInput,Snackbar.LENGTH_LONG)
                        .setAction(R.string.understood,View.OnClickListener {
                        // executed when DISMISS is clicked
                        System.out.println("Snackbar Set Action - OnClick.")
                    })
                    .setActionTextColor(Color.parseColor("#F03C15"))
                    .show()
                "PASSWORD_LESS_THAN_8_OR_MORE_THAN_16" -> Snackbar.make(view.findViewById(R.id.loginLayout), "La contraseña debe tener entre 8 y 16 caracteres",Snackbar.LENGTH_LONG)
                    .setAction(R.string.understood,View.OnClickListener {
                        // executed when DISMISS is clicked
                        System.out.println("Snackbar Set Action - OnClick.")
                    })
                    .setActionTextColor(Color.parseColor("#F03C15"))
                    .show()
                "SUCCESS_LOGIN" ->
                {
                    Thread {
                        if (LoginCheck(inputEmail.editText?.text.toString(),inputPassword.editText?.text.toString())) {
                            goToMenuPrincipal()
                        }else Snackbar.make(view.findViewById(R.id.loginLayout),
                            R.string.wrongUser,Snackbar.LENGTH_LONG)
                            .setAction(R.string.understood,View.OnClickListener {
                                // executed when DISMISS is clicked
                                System.out.println("Snackbar Set Action - OnClick.")
                            })
                            .setActionTextColor(Color.parseColor("#F03C15"))
                            .show()
                    }.start()
                } else -> {

                }
            }

        }
    }

    //Esto se ejecuta al comienzo si el booleano esta en true ingresa automaticamente entonces si cierra la aplicacion se meantendra logeado
    //hasta cerrar sesion
    override fun onStart() {
        super.onStart()
        if (isLogged()) {
            goToMenuPrincipal()
        }
    }

    //Iremos a menu principal donde esta toda la funcionalidad de la aplicacion
    fun goToMenuPrincipal(){
        findNavController().navigate(R.id.MenuPrincipal, null)
    }


    fun isLogged(): Boolean{
        return preferences.getBoolean(IS_LOGGED, false)
    }


    private fun LoginCheck(Email: String, Password: String): Boolean {
        val client = OkHttpClient()
        val formBody: RequestBody = FormBody.Builder()
            .add("email", Email)
            .add("password", Password)
            .build()

        val request = Request.Builder()
            .url(Url)
            .post(formBody)
            .build()

        try {
            val response = client.newCall(request).execute()
            val body = response.body?.string()

            Log.d("Response: ", body.toString())
            val json = JSONObject(body)

            if (json.has("error")) {
                preferences.edit()
                    .putBoolean(IS_LOGGED, false)
                    .apply()
            } else {
                getUserId(Email)
            }
        } catch (e: Error){
            Log.e("Error",e.toString())
        }
        return isLogged()
    }


    fun getUserId(Email: String){
        val client = OkHttpClient()
        val request = Request.Builder()
            .url(Url2)
            .build()
        val response = client.newCall(request).execute()
        val body = response.body?.string()

        Log.d("Response: ", body.toString())

        val json = JSONObject(body)
        for (i in 0 until json.getJSONArray("data").length()){
            if (json.getJSONArray("data").getJSONObject(i).getString("email") == Email) {
                preferences.edit()
                    .putString(JSON,json.getJSONArray("data").getJSONObject(i).toString())
                    .putInt("ID", i)
                    .putBoolean(IS_LOGGED,true)
                    .apply()
            }
        }
    }


}