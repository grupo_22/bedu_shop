package org.bedu.shopbedu.fragments

import android.os.Bundle
import android.util.TypedValue
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.squareup.picasso.Picasso
import io.realm.Realm
import org.bedu.shopbedu.R
import org.bedu.shopbedu.models.Carrito
import org.bedu.shopbedu.MyProductItem
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class DetailFragment : Fragment() {
    private val args: DetailFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //animacion de transition desde listfragment
        val animation = TransitionInflater.from(requireContext()).inflateTransition(R.transition.transition_shared)
        sharedElementEnterTransition = animation
        sharedElementReturnTransition = animation  //andaria si no tendriamos que recargar la pantalla!!
    }

    private lateinit var productName: TextView
    private lateinit var productDescription: TextView
    private lateinit var rbRate: RatingBar
    private lateinit var imgProduct: ImageView
    private lateinit var productPrice: TextView
    private lateinit var useCondition : TextView
    private lateinit var quotaPrice : TextView
    private lateinit var ratingVal:TextView
    private lateinit var realm: Realm
    private lateinit var btnCart : Button
    private lateinit var productoEntrega: TextView

    //Instanciamos y recibimos el producto utilizando safeargs.


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //Inflamos el fragmento
        val bottomNav = requireActivity().findViewById(R.id.bottomMenu) as  BottomNavigationView
        val view = inflater.inflate(R.layout.detail_fragment, container, false)
        //Obtenemos los diferentes elementos desde el layout
        productName = view.findViewById(R.id.product_name)
        productDescription = view.findViewById(R.id.product_description)
        rbRate = view.findViewById(R.id.ratingBar_detalle)
        imgProduct = view.findViewById(R.id.imgProduct)
        productPrice = view.findViewById(R.id.product_price)
        useCondition = view.findViewById(R.id.condicion_Uso)
        quotaPrice = view.findViewById(R.id.valor_cuota)
        ratingVal = view.findViewById(R.id.rtb_Detail)
        btnCart = view.findViewById(R.id.btn_agregar)
        productoEntrega = view.findViewById(R.id.producto_entrega)

        realm = Realm.getDefaultInstance()
        //Modifico el tamaño de fuente del Boton
        btnCart.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15F)

        btnCart.setOnClickListener {
            addCarrito(args.product)
            bottomNav.selectedItemId = R.id.carrito_dest;
            findNavController().navigate(R.id.carritoFragment)
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showProduct(args.product)
    }

    // Define como mostrar el producto en el fragment_detail.xm !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    fun showProduct(product: MyProductItem){
        //Display deliver date
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, 1)
        val tomorrow = calendar.time

        val dateFormat = SimpleDateFormat("dd-MM-yyyy")
        //Formato para truncado a dos números decimales
        val df = DecimalFormat("####.##")
        df.roundingMode = RoundingMode.CEILING
        //Asignamos valores a elementos del Layout
        useCondition.text= product.category //antes estaba como nuevo
        productName.text = product.title
        rbRate.rating = product.rating!!.rate.toFloat()
        ratingVal.text = product.rating.count.toString()
        Picasso.get().load(product.image).into(imgProduct)
        productPrice.text = getText(R.string.moneySign).toString() + product.price.toString()
        quotaPrice.text = getText(R.string.moneySign).toString() + (df.format(product.price / 6)).toString()
        productDescription?.text = product.description
        view?.transitionName = product.title
        productoEntrega.text = getText(R.string.deliveryTomorrow).toString() + "${(dateFormat.format(tomorrow))}"
    }



    //Sumamos un Porduct en el Mustablelist que guardamos localmente
    fun addCarrito(product: MyProductItem){
        realm.beginTransaction()
        var flag = true
        val carritoList = realm.where(Carrito::class.java).findAll()
        val carrito = Carrito()

        carritoList.forEach{
            if(product.id == it.id){
                it.stock = it.stock?.plus(1)
                flag = false
            }
        }
        if(flag){
            carrito.id = product.id
            carrito.title = product.title
            carrito.price = product.price
            carrito.image = product.image
            carrito.stock = 1
            realm.copyToRealmOrUpdate(carrito)
        }
        //commitTranaction es necesario SIEMPRE para cerrar una transacion
        realm.commitTransaction()
        Toast.makeText(context, R.string.addProduct,Toast.LENGTH_LONG).show()
    }
}

