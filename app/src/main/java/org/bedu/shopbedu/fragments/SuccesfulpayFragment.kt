package org.bedu.shopbedu.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.bedu.shopbedu.R


class SuccesfulpayFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val bottomNav = requireActivity().findViewById(R.id.bottomMenu) as  BottomNavigationView
        val view = inflater.inflate(R.layout.successfulpay_fragment, container, false)
        val btnmenu = view.findViewById(R.id.btn_menu_principal) as Button

       //Simplemente navega
        btnmenu.setOnClickListener {
            bottomNav.selectedItemId = R.id.home_dest
            findNavController().navigate(R.id.listFragment)
        }
        return view
    }
}
