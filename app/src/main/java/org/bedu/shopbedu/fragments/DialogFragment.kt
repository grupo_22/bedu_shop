package org.bedu.shopbedu.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.bedu.shopbedu.R
import java.util.*

class DialogFragment : BottomSheetDialogFragment() {

    companion object {
        const val PERMISSION_ID = 33
    }

    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private lateinit var updateButton: TextView
    private lateinit var currentAddressLine: TextView
    private lateinit var addDirection: TextView
    private lateinit var listView: ListView
    private lateinit var closeButton: ImageButton


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        return inflater.inflate(R.layout.dialog_fragment, container, false)
    }

    @SuppressLint("ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateButton = view.findViewById(R.id.updateBtn)
        currentAddressLine = view.findViewById(R.id.addressLine)
        listView = view.findViewById(R.id.addressList)
        addDirection = view.findViewById(R.id.addDirection)
        closeButton = view.findViewById(R.id.closeButton)
        updateButton.setOnClickListener {
            getActualLocation() // obtenemos la localización actual
        }
        closeButton.setOnClickListener {
            dismiss()
        }
        val itemsAdapter = ArrayAdapter<String>(requireContext(),android.R.layout.simple_list_item_1)
        listView.adapter = itemsAdapter

        addDirection.setOnClickListener {
            if(currentAddressLine.text.isNullOrEmpty()){
                Toast.makeText(requireContext(),R.string.updateLocation, Toast.LENGTH_SHORT).show()
            }else{
                itemsAdapter.add(currentAddressLine.text.toString())
            }
        }
    }


    private fun checkGranted(permission: String): Boolean {
        return ActivityCompat.checkSelfPermission(
            requireActivity(),
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun checkPermissions(): Boolean {
        if (checkGranted(Manifest.permission.ACCESS_COARSE_LOCATION) &&
            checkGranted(Manifest.permission.ACCESS_FINE_LOCATION)
        ) {
            return true
        }
        return false
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            requireActivity().getSystemService(LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    @SuppressLint("MissingPermission")
    private fun getActualLocation() {
        val geocoder = Geocoder(requireActivity(), Locale.getDefault())

        if (checkPermissions()) { //verificamos si tenemos permisos
            if (isLocationEnabled()) { //localizamos sólo si el GPS está encendido
                mFusedLocationClient.lastLocation.addOnSuccessListener(requireActivity()) { location ->
                    if (location != null) {
                        val addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1)
                        currentAddressLine.text = addresses[0].getAddressLine(0)
                        Log.d("Current Address Line", addresses[0].getAddressLine(0))
                    }else{
                        Toast.makeText(requireContext(),R.string.setLocation, Toast.LENGTH_SHORT).show()
                    }

                }
                }
            } else {
                requestPermissions()
            }
        }

        //Pedir los permisos requeridos para que funcione la localización
        fun requestPermissions() {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                PERMISSION_ID
            )
        }


    }
