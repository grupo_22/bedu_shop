package org.bedu.shopbedu.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import io.realm.Realm
import io.realm.RealmResults
import org.bedu.shopbedu.R
import org.bedu.shopbedu.adapters.RecyclerAdapterCarrito
import org.bedu.shopbedu.models.Carrito


class CarritoFragment: Fragment() , RecyclerAdapterCarrito.OnCarritoClickListener {

    private lateinit var realm: Realm
    private lateinit var recycler : RecyclerView
    private lateinit var adapter : RecyclerAdapterCarrito
    private lateinit var btnLogin : Button
    private lateinit var carritoIsEmpty : TextView



    //Mostramos El fragment
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.carrito_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        realm = Realm.getDefaultInstance()
        super.onViewCreated(view, savedInstanceState)
        btnLogin = view.findViewById(R.id.pay_carrito)
        carritoIsEmpty = view.findViewById(R.id.carritoIsEmpty)
        //Muestra el Recycler
        setUpRecyclerView()
        //verifica si esta vacio el carrito para mostrar un mensaje
        carritoIsEmpty()

        btnLogin.setOnClickListener {
            findNavController().navigate(R.id.action_carritoFragment_to_metodoFragment)
        }
    }

    private fun setUpRecyclerView(){
        recycler = view?.findViewById(R.id.recyclerCarrito) as RecyclerView
        adapter = RecyclerAdapterCarrito(requireActivity(),getCarrito(),this)
        recycler.adapter = adapter
    }


    //Obtenemos la lista que esta localmente guardado
    private fun getCarrito():  MutableList<Carrito> {
        val realm =  Realm.getDefaultInstance()
        val results:RealmResults<Carrito> = realm.where(Carrito::class.java).findAll()
        return results
    }

    //comprobamos si la lista local esta vacia
    private fun carritoIsEmpty(){
        if(getCarrito().isEmpty()){
            btnLogin.visibility =  View.INVISIBLE
            carritoIsEmpty.visibility = View.VISIBLE
        }else {
            btnLogin.visibility =  View.VISIBLE
            carritoIsEmpty.visibility = View.INVISIBLE
        }
    }

    //click sobre icono Next
    override fun onNextClick(carrito: Carrito) {
        realm.beginTransaction()
        val carritoList = realm.where(Carrito::class.java).findAll()
        carritoList.forEach{
            if(carrito.id == it.id){
                it.stock = it.stock?.plus(1)
            }
        }
        realm.commitTransaction()
        adapter.notifyDataSetChanged()
    }

    //click sobre icono Before
    override fun onBeforeClick(carrito: Carrito) {
        realm.beginTransaction()
        if (carrito.stock == 1){
            val carritoList = realm.where(Carrito::class.java).findAll()
            for (i in 0..(carritoList.size-1)){
                if(carrito.id == carritoList[i]!!.id){
                    adapter.notifyItemRemoved(i)
                }
            }
            carrito.deleteFromRealm()
            realm.commitTransaction()
            carritoIsEmpty()
        }else{
            val carritoList = realm.where(Carrito::class.java).findAll()
            carritoList.forEach{
                if(carrito.id == it.id){
                    it.stock = it.stock?.minus(1)
                }
            }
            realm.commitTransaction()
            adapter.notifyDataSetChanged()
        }
    }

    //click sobre icono Delete
    override fun onDeleteClick(carrito: Carrito) {
        realm.beginTransaction()
        val carritoList = realm.where(Carrito::class.java).findAll()
        for (i in 0..(carritoList.size-1)){
            if(carrito.id == carritoList[i]!!.id){
                adapter.notifyItemRemoved(i)
            }
        }
        carrito.deleteFromRealm()
        realm.commitTransaction()
        carritoIsEmpty()
    }
}