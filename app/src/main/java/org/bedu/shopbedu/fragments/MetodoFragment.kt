package org.bedu.shopbedu.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import org.bedu.shopbedu.R
import org.bedu.shopbedu.adapters.RecyclerAdapterCard
import org.bedu.shopbedu.adapters.RecyclerAdapterMetod
import org.bedu.shopbedu.models.Card
import org.bedu.shopbedu.models.Imagen
import org.bedu.shopbedu.models.Metod

class MetodoFragment : Fragment(),  RecyclerAdapterCard.OnCardClickListener  , RecyclerAdapterMetod.OnMetodClickListener{
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.metododepago_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        setUpRecyclerView2()
    }


    //mostramos el recycler de cards
    private fun setUpRecyclerView(){
        val recycler = view?.findViewById(R.id.recyclerCard) as RecyclerView
        //HARD CODING PAPU
        recycler.adapter =  RecyclerAdapterCard(requireActivity(),listOf(
            Card(R.color.common_google_signin_btn_text_dark, R.color.white),
            Card(R.color.white, R.color.black),
            Card(R.color.black, R.color.white),
        ),this)
    }

    //mostramos el recycler de metod
    private fun setUpRecyclerView2(){
        val recycler = view?.findViewById(R.id.recyclerMetodo) as RecyclerView
        //HARD CODING PAPU
        recycler.adapter =  RecyclerAdapterMetod(requireActivity(),listOf(
            Metod(R.drawable.rapipago),
            Metod(R.drawable.mercadopago),
            Metod(R.drawable.pf),
            Metod(R.drawable.naranja_x),
            Metod(R.drawable.ce),
            Metod(R.drawable.provincianet),
            Metod(R.drawable.banelco),
            Metod(R.drawable.link),
        ),this)
    }

    //click lisener  de card cuando se clickea en un card y se necesita enviar informacion
    override fun onItemClick(card: Card) {
        val imagen = Imagen()
        imagen.card = card
        val direction = MetodoFragmentDirections.actionMetodoFragmentToPayFragment(imagen = imagen)
        findNavController().navigate(direction)
    }

    //click lisener  de card cuando se clickea en un metody se necesita enviar informacion
    override fun onItemClick(metodos: Metod) {
        val imagen = Imagen()
        imagen.metod = metodos
        val direction = MetodoFragmentDirections.actionMetodoFragmentToPayFragment(imagen = imagen)
        findNavController().navigate(direction)
    }
}