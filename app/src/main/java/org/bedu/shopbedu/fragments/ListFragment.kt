package org.bedu.shopbedu.fragments



import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import okhttp3.*
import org.bedu.shopbedu.interfaces.ApiInterface
import org.bedu.shopbedu.R
import org.bedu.shopbedu.adapters.RecyclerAdapterProduct
import org.bedu.shopbedu.MyProductItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class ListFragment : Fragment() {

    //Es necesario crear una lista vacia para poder mostrar algo y no vacio en el recyclerview entonces empieza vacio hasta que lo llenemos con datos
    private var productlist: MutableList<MyProductItem> = mutableListOf()
    private lateinit var recycler : RecyclerView
    private lateinit var adapter : RecyclerAdapterProduct

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getDatos()
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
    }

    private fun setUpRecyclerView(){
        recycler = view?.findViewById(R.id.recyclerProducts) as RecyclerView
        adapter = RecyclerAdapterProduct( productlist )
        adapter.itemClickListener = object : RecyclerAdapterProduct.OnProductClickListener {
            override fun onProductClick(product: MyProductItem, imageView: ImageView) {
                val extras = FragmentNavigatorExtras(
                    imageView to "imgProduct"
                )
                val direction =  ListFragmentDirections.actionListFragmentToDetailFragment(product)
                findNavController().navigate(direction, extras)
            }
        }
        recycler.adapter = adapter
    }

    private fun getRetrofit() : ApiInterface {
        val client = OkHttpClient.Builder()
            .readTimeout(2,TimeUnit.SECONDS)
            .build()

        return Retrofit.Builder()
            .baseUrl("https://fakestoreapi.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(ApiInterface::class.java)
    }

    //Llamada a la api para obtener la lista de producto y ponerlos en nuestro productlist
    private fun getDatos(){
        val progressbar = view?.findViewById(R.id.progressBarListProduct) as? ProgressBar
        val call = getRetrofit().getProducts()
        call.enqueue(object : Callback<MutableList<MyProductItem>> {
            override fun onResponse(call: Call<MutableList<MyProductItem>>, response: Response<MutableList<MyProductItem>>) {
                response.body()?.let {
                    if (response.isSuccessful){
                            adapter.setValue(response.body()!!)
                            Log.d("onResponse: ", response.toString())
                        }
                }
                progressbar?.visibility =  View.INVISIBLE
            }
            override fun onFailure(call: Call<MutableList<MyProductItem>>, t: Throwable) {
                Log.d("onFailure: ", t.toString())
                getDatos()
            }
        })
    }
}