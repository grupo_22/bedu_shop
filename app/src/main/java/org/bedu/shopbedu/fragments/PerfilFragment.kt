package org.bedu.shopbedu.fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.perfil_fragment.*
import org.bedu.shopbedu.MainActivity
import org.bedu.shopbedu.R
import org.bedu.shopbedu.adapters.RecyclerAdapterElementos
import org.bedu.shopbedu.models.Elemento
import org.json.JSONObject

class PerfilFragment: Fragment(), RecyclerAdapterElementos.OnElementosClickListener {

    lateinit var preferences: SharedPreferences


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.perfil_fragment, container, false)
        override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        setJsonData()
    }


    //Mostramos el recycler de elementos
    private fun setUpRecyclerView(){
        val recycler = view?.findViewById(R.id.recyclerElementos) as RecyclerView
        //HARD CODING PAPU
        recycler.adapter = RecyclerAdapterElementos(requireActivity(),listOf(
            Elemento(R.drawable.location, getText(R.string.myAdresses).toString()),
            Elemento(R.drawable.card, getText(R.string.payMethod).toString()),
            Elemento(R.drawable.timer, getText(R.string.orders).toString()),
            Elemento(R.drawable.notifications, getText(R.string.notifications).toString()),
            Elemento(R.drawable.padlock_black, getText(R.string.changePassword).toString()),
            Elemento(R.drawable.logout, getText(R.string.logOut).toString()),
            ),this)
    }


    //Dependiendo de que elemento se de click elejimos una accion
    override fun onItemClick(elementos: Elemento) {
        when (elementos.text){
            getText(R.string.myAdresses).toString()->{
                val fragment = DialogFragment()
                fragment.show(childFragmentManager , fragment.tag)
            }
            getText(R.string.payMethod).toString()->{
                findNavController().navigate(R.id.action_perfilFragment_to_metodoFragment)
            }

            getText(R.string.orders).toString()->{
                findNavController().navigate(R.id.action_perfilFragment_to_metodoFragment)
            }
            getText(R.string.notifications).toString()->{

            }
            getText(R.string.changePassword).toString()->{

            }
            getText(R.string.logOut).toString()->{
                setValues()
                goToLogged()

            }
        }
    }

    //obtenemos los datos que tenemos localmente
    fun setJsonData(){
        preferences =  requireActivity().getSharedPreferences(LoginFragment.PREFS_NAME, Context.MODE_PRIVATE)
        val body = preferences.getString(LoginFragment.JSON,"")
        val json = JSONObject(body)
        val imagenpapu = json.getString("avatar").replace("\\","/") //ES un forro y lo tuvimos que poner asi porque nos cada ves que encontraba un / agregaba \
        Picasso.get().load(imagenpapu).into(perfil_img)
        usuario.text = json.getString("first_name") + " " + json.getString("last_name")
        email.text = json.getString("email")
    }

    //editamos los datos locales
    fun setValues(){
        preferences =  requireActivity().getSharedPreferences(LoginFragment.PREFS_NAME, Context.MODE_PRIVATE)
        preferences.edit()
            .putBoolean(LoginFragment.IS_LOGGED, false)
            .apply()
    }

    //para delogear
    fun goToLogged(){
        //cambiar de actividad sin poder regresar a esta con back button
        val i = Intent(activity, MainActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
    }
}