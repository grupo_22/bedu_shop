package org.bedu.shopbedu.fragments

//Singleton object para crear la funcionalidad de validar email y password inputs.
object LoginChecker {
    fun validateLoginInput(email: String,password: String): String {
        if (password.isEmpty() || email.isEmpty()) {
            return "EMPTY_FIELDS"
        }
        if (password.length !in 8..16) {
            return "PASSWORD_LESS_THAN_8_OR_MORE_THAN_16"
        }
        return "SUCCESS_LOGIN"
    }
}