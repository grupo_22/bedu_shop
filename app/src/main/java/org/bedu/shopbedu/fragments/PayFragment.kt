package org.bedu.shopbedu.fragments

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavDeepLinkBuilder
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import io.realm.Realm
import kotlinx.android.synthetic.main.pay_fragment.*
import org.bedu.shopbedu.R
import org.bedu.shopbedu.adapters.RecyclerAdapterElementos
import org.bedu.shopbedu.models.Carrito
import org.bedu.shopbedu.models.Elemento
import java.math.RoundingMode
import java.text.DecimalFormat


class PayFragment : Fragment(), RecyclerAdapterElementos.OnElementosClickListener {

    private lateinit var realm: Realm
    private lateinit var products: TextView
    private lateinit var send: TextView
    private lateinit var total: TextView
    private lateinit var btnLogin: Button
    private val CHANNEL_OTHERS = "OTROS"
    private lateinit var spinnerCuota : Spinner
    private lateinit var imageMetod: ImageView

    //datos enviados desde metodoFragmet
    private val args: PayFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        // We set the notification channel (On Android Oreo+ is a must)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setNotificationChannel()
        }
        return inflater.inflate(R.layout.pay_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        realm = Realm.getDefaultInstance()
        products = view.findViewById(R.id.pay_products)
        send = view.findViewById(R.id.pay_send)
        total = view.findViewById(R.id.pay_total)
        btnLogin = view.findViewById(R.id.btn_pay_pay)
        imageMetod = view.findViewById(R.id.imageMetod)
        spinnerCuota = view.findViewById(R.id.SpinnerValorCuota)
        setImageMetod()
        setValues()
        setUpRecyclerView()

        //se envia el mensaje y se borra carrito
        btn_pay_pay.setOnClickListener {
            realm.beginTransaction()
            val realm =  Realm.getDefaultInstance()
            realm.deleteAll()
            realm.commitTransaction()
            findNavController().navigate(R.id.action_payFragment_to_succesfulpayFragment)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                actionNotification()
            }
        }
    }

    //creamos una instancia de get para obtener los datos guardados localmente esto es para reducir codigo
    private fun getTotalPirce(): Double? {
        val carritoList = realm.where(Carrito::class.java).findAll()
        var total: Double? = 0.0
        if (total != null) {
            for (i in 0..carritoList.size - 1) {
                total += carritoList[i]?.price!!
            }
        }
        return total
    }

    //mostramos el recycler de elemetos
    private fun setUpRecyclerView() {
        val recycler = view?.findViewById(R.id.recyclerpay) as RecyclerView
        //HARD CODING PAPU
        recycler.adapter = RecyclerAdapterElementos(
            requireActivity(), listOf(
                Elemento(R.drawable.location,  getText(R.string.myAdresses).toString()),
                Elemento(R.drawable.card, getText(R.string.payMethod).toString()),
            ), this
        )
    }

    //damos valor a la vista
    private fun setValues() {
        val df = DecimalFormat("####.##")
        df.roundingMode = RoundingMode.CEILING
        val get_price = getTotalPirce()
        val send_price = 30.0
        val total_price = get_price?.plus(send_price)

        products.text = getText(R.string.moneySign).toString() + (df.format(get_price).toString())
        send.text = getText(R.string.moneySign).toString() + send_price.toString()
        if (total_price != null) {
            total.text = getText(R.string.moneySign).toString() + (df.format(total_price )).toString()
        }

        spinnerCuota.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            }
        }

        val valorCuota = arrayOf(getText(R.string.cuota1).toString() +" $ "+ (df.format(total_price).toString()),
            getText(R.string.cuota2).toString() +" $ "+ (df.format(total_price?.div(2)).toString()),
            getText(R.string.cuota3).toString() +" $ "+ (df.format(total_price?.div(3)).toString()),
            getText(R.string.cuota4).toString() +" $ "+ (df.format(total_price?.div(4)).toString()),
            getText(R.string.cuota5).toString() +" $ "+ (df.format(total_price?.div(5)).toString()),
            getText(R.string.cuota6).toString() +" $ "+ (df.format(total_price?.div(6)).toString()))


        val cuotaAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1,valorCuota)
        cuotaAdapter.setDropDownViewResource((android.R.layout.simple_spinner_dropdown_item))
        spinnerCuota.adapter= cuotaAdapter
    }


    //Lisener para volver a metodo de pago
    override fun onItemClick(elementos: Elemento) {
        when (elementos.text) {
            getText(R.string.myAdresses).toString() -> {
                val fragment = DialogFragment()
                fragment.show(childFragmentManager, fragment.getTag())
            }
            getText(R.string.payMethod).toString() -> {
                findNavController().navigate(R.id.action_payFragment_to_metodoFragment)
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun setNotificationChannel() {
        val name = getString(R.string.notification_action_title)
        val descriptionText = getString(R.string.notification_action_body_first)
        val importance = NotificationManager.IMPORTANCE_DEFAULT

        val channel = NotificationChannel(CHANNEL_OTHERS, name, importance).apply {
            description = descriptionText
        }

        val notificationManager =
            context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.createNotificationChannel(channel)
    }

    @SuppressLint("ResourceAsColor")
    private fun actionNotification() {

        val pendingIntent = NavDeepLinkBuilder(requireContext())
            .setComponentName(requireActivity()::class.java)
            .setGraph(R.navigation.navigation)
            .setDestination(R.id.succesfulpayFragment)
            .createPendingIntent()

        val builder = NotificationCompat.Builder(requireContext(), CHANNEL_OTHERS)
            .setSmallIcon(R.drawable.shopping_bag) //seteamos el ícono de la push notification (aún no implementado)
            .setColor(ContextCompat.getColor(requireContext(), R.color.naranja))
            .setContentTitle(getString(R.string.notification_action_title)) //seteamos el título de la notificación
            .setStyle(NotificationCompat.BigTextStyle().bigText(getString(R.string.notification_action_body_first) + "\n" + getString(
                R.string.notification_action_body_second
            )))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT) // Asignamos una prioridad por defecto
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        //lanzamos la notificación
        with(NotificationManagerCompat.from(requireContext())) {
            notify(0, builder.build()) //en este caso pusimos un id genérico
        }
    }

    fun setImageMetod(){

        if ( args.imagen.card == null){
            args.imagen.metod?.let { imageMetod.setImageResource(it.logoImage) }
        }else{
            imageMetod.setImageResource(R.drawable.mastercard)
        }
        Log.d("Imagen: ", args.imagen.card.toString())
    }
}