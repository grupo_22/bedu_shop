package org.bedu.shopbedu.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.SignInButton
import com.google.android.material.textfield.TextInputLayout
import okhttp3.OkHttpClient
import okhttp3.Request
import org.bedu.shopbedu.R
import org.json.JSONObject

const val USER_EMAIL = "org.bedu.activities.USER_EMAIL"

class RegisterFragment : Fragment() {

    private lateinit var textNombre : TextInputLayout
    private lateinit var textCorreo : TextInputLayout
    private lateinit var textTelefono : TextInputLayout
    private lateinit var textConstraseña : TextInputLayout
    private lateinit var buttonRegistro : Button
    private lateinit var buttonGoogle: SignInButton
    lateinit var preferences: SharedPreferences

    val duration = Toast.LENGTH_LONG

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Inflamos el fragmento
        return inflater.inflate(R.layout.register_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        textNombre = view.findViewById(R.id.nombre)
        textCorreo = view.findViewById(R.id.correo)
        textTelefono = view.findViewById(R.id.telefono)
        textConstraseña = view.findViewById(R.id.contraseña)
        buttonGoogle = view.findViewById(R.id.btnGoogle)
        buttonRegistro = view.findViewById(R.id.btn_iniciarSesion)

        preferences =  requireActivity().getSharedPreferences(LoginFragment.PREFS_NAME, Context.MODE_PRIVATE)

        buttonRegistro.setOnClickListener {
            if(textNombre.editText?.text.toString().isNotEmpty() && textCorreo.editText?.text.toString().isNotEmpty() && textTelefono.editText?.text.toString().isNotEmpty() &&  textConstraseña.editText?.text.toString().isNotEmpty()) {
                findNavController().navigate(R.id.action_register_to_loginFragment)
            } else Toast.makeText(context, R.string.wrongInput, duration).show()
        }

        buttonGoogle.setOnClickListener {
            Thread {
                getUserId()
                    findNavController().navigate(R.id.MenuPrincipal, null)
            }.start()

        }
    }

    //Cuando ingresamos con el boton de google ingresmaos con un usuario random entre 1 y 12
    fun getUserId(){
        val client = OkHttpClient()
        val random = (1..12).random()
        val Url2= "https://reqres.in/api/users/" + random.toString()

        val request = Request.Builder()
            .url(Url2)
            .build()

        val response = client.newCall(request).execute()
        val body = response.body?.string()
        val json = JSONObject(body)

        preferences.edit()
            .putString(LoginFragment.JSON,json.getJSONObject("data").toString())
            .putInt("ID",random)
            .putBoolean(LoginFragment.IS_LOGGED,true)
            .apply()
    }
}





