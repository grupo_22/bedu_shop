package org.bedu.shopbedu

import android.os.Parcel
import android.os.Parcelable
import org.bedu.shopbedu.models.Rating

data class MyProductItem(
    val id: Int,
    val title: String?,
    val category: String?,
    val description: String?,
    val image: String?,
    val price: Double,
    val rating: Rating?,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readParcelable(Rating::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(category)
        parcel.writeString(description)
        parcel.writeString(image)
        parcel.writeDouble(price)
        parcel.writeParcelable(rating, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MyProductItem> {
        override fun createFromParcel(parcel: Parcel): MyProductItem {
            return MyProductItem(parcel)
        }

        override fun newArray(size: Int): Array<MyProductItem?> {
            return arrayOfNulls(size)
        }
    }
}