package org.bedu.shopbedu.models

import android.os.Parcel
import android.os.Parcelable


data class Metod (
    val logoImage: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(logoImage)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Metod> {
        override fun createFromParcel(parcel: Parcel): Metod {
            return Metod(parcel)
        }

        override fun newArray(size: Int): Array<Metod?> {
            return arrayOfNulls(size)
        }
    }
}