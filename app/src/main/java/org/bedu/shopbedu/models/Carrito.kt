package org.bedu.shopbedu.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Carrito : RealmObject() {
    @PrimaryKey
    var id: Int? = null

    var image: String? = null

    var title: String? = null

    var price: Double? = null

    var stock: Int? = null
}