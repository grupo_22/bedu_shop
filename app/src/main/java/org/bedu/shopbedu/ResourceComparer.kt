package org.bedu.shopbedu

import android.content.Context

//Clase que para crear la funcionalidad que testee los recursos de la aplicacion
class ResourceComparer {

    fun isEqual (context: Context, resId:Int, string:String) :Boolean{
        return context.getString(resId) == string
    }

}