package org.bedu.shopbedu.fragments

import com.google.common.truth.Truth.assertThat
import org.junit.Test

//Clase para testear la funcion de Login
class LoginCheckerTest {
    //Se testea si la contrasenia esta vacio
    @Test
    fun `Empty Password returns false`() {
        val result = LoginChecker.validateLoginInput(
            "","asdasd"
        )
        assertThat(result).contains("EMPTY_FIELDS")
    }

    @Test
    fun `Empty Mail Empty Password returns false`() {
        val result = LoginChecker.validateLoginInput(
            "",""
        )
        assertThat(result).contains("EMPTY_FIELDS")
    }
    //Se testea si el email esta vacio
    @Test
    fun `Empty Mail returns false`() {
        val result = LoginChecker.validateLoginInput(
            "asdasdad",""
        )
        assertThat(result).contains("EMPTY_FIELDS")

    }
    //Se testea si la contrasenia contiene al menos 8 caracteres
    @Test
    fun `Password must have more than 8 characters`() {
        val result = LoginChecker.validateLoginInput(
            "12345","asdasd"
        )
        assertThat(result).contains("PASSWORD_LESS_THAN_8_OR_MORE_THAN_16")
    }
    //Se testea si la contrasenia tiene mas de 16 caracteres
    @Test
    fun `Password must not have more than 16 characters`() {
        val result = LoginChecker.validateLoginInput(
            "123456789101112131415","asd"
        )
        assertThat(result).contains("PASSWORD_LESS_THAN_8_OR_MORE_THAN_16")
    }

}